package service;

import model.Books;
import model.DBPersist;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;

public class convertToObject {
    private static Logger logger=Logger.getLogger(convertToObject.class);

    public convertToObject() {
    }


    //Validate the given XML string and save in a database.
    public boolean validate(String xmlPload){

        JAXBContext jaxbContext;
        try{
            String xsd= "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                    "<xs:schema version=\"1.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">\n" +
                    "\n" +
                    "  <xs:element name=\"books\" type=\"books\"/>\n" +
                    "\n" +
                    "  <xs:complexType name=\"books\">\n" +
                    "    <xs:sequence>\n" +
                    "      <xs:element name=\"id\" type=\"xs:string\" minOccurs=\"0\"/>\n" +
                    "      <xs:element name=\"name\" type=\"xs:string\" minOccurs=\"0\"/>\n" +
                    "    </xs:sequence>\n" +
                    "  </xs:complexType>\n" +
                    "</xs:schema>\n" +
                    "\n";
            jaxbContext=JAXBContext.newInstance(Books.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory sf=SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema bookSchema=sf.newSchema(new StreamSource(new StringReader(xsd)));
            jaxbUnmarshaller.setSchema(bookSchema);
            Validator validator=bookSchema.newValidator();
            validator.validate(new StreamSource(new StringReader(xmlPload)));
            Books book = (Books) jaxbUnmarshaller.unmarshal(new StringReader(xmlPload));
            DBPersist.persist(book);
            return true;
        } catch (JAXBException | SAXException | IOException e){
            logger.error(e);
        }
        return false;
    }

    //convert a given XML string to a JSON string
    public String convert(String xmlPload) {
        int P_P_I_F=4;              //Pretty_Print_Indentation_Factor
        try {
            JSONObject xmlJsonObject= XML.toJSONObject(xmlPload);
            return xmlJsonObject.toString(P_P_I_F);
        }catch (JSONException je){
            logger.error(je);
            return "";
        }

    }
}
