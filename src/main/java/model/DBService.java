package model;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import javax.sql.DataSource;

public class DBService {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String JDBC_DB_URL = "jdbc:mysql://localhost:3306/TEST";

    static final String USER = "intern";
    static final String PASS = "1234";

    private static GenericObjectPool gPool = null;
    private static DBService dbService;
    private static DataSource dataSource=null;

    private static Logger logger=Logger.getLogger(DBService.class);

    private DBService(){

    }

    public static DBService getInstance(){
        if(dbService==null){
            dbService=new DBService();
        }
        return dbService;
    }


    //Creates a connection pool
    public DataSource setUpPool() throws Exception{
        if(dataSource==null) {
            Class.forName(JDBC_DRIVER);

            gPool = new GenericObjectPool();
            gPool.setMaxActive(5);

            ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, USER, PASS);

            PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
            logger.info("Connection Pool Created");
            dataSource= new PoolingDataSource(gPool);
        }
        return dataSource;
    }

    public GenericObjectPool getConnectionPool(){
        return gPool;
    }

    public void printDbStatus(){
        logger.info("Max: " + getConnectionPool().getMaxActive() +
                "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
    }

}
