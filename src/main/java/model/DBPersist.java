package model;

import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBPersist {

    private static Logger logger=Logger.getLogger(DBPersist.class);

    //Add the given Books object to database
    public static void persist(Books book){
        Connection conn=null;
        PreparedStatement pstmt=null;
        DBService dbService= DBService.getInstance();

        try {
            DataSource dataSource=dbService.setUpPool();
            dbService.printDbStatus();
            conn=dataSource.getConnection();

            pstmt=conn.prepareStatement("insert into book (id,name) values (?,?)");
            pstmt.setString(1,book.getId());
            pstmt.setString(2,book.getName());
            pstmt.executeUpdate();
        } catch (Exception e) {
            logger.error(e);
        }finally {
            try {
                if(pstmt!=null){
                    pstmt.close();
                }if(conn!=null){
                    conn.close();
                }
            } catch (SQLException e) {
                logger.error(e);
            }
        }
    }
}
