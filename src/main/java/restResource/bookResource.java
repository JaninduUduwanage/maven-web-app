package restResource;

import service.convertToObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/books")
public class bookResource {
    private convertToObject cto=new convertToObject();

    //Method for test the functionality.
    @Path("/ok")
    @GET
    public Response sayOk(){
        return Response.ok().build();
    }

    //Method for validate a given XML string against a known xsd and persist in a database.
    @Path("/validate")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response sendBackValid(String pld) {
        if (cto.validate(pld)) {
            return Response.status(200).entity(pld).build();
        }
        else {
            return Response.status(400).entity("Invalid data").build();
        }
    }

    //Method to convert a well formed XML to JSON
    @Path("/convert")
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendBackConvert(String pld){
        String resultString=cto.convert(pld);
        if(!resultString.isEmpty()){
            return Response.status(200).entity(resultString).build();
        }else {
            return Response.status(400).entity("Invalid data").build();
        }
    }
}
